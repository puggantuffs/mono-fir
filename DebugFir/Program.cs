﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FirApi;

namespace DebugFir
{
    class MainClass
    {
        PlayerToken token = null;
        String username = null;
        FirApi.Api api;

        #region Main/Init/Start
        public static void Main(string[] args)
        {
            MainClass self;
            if(args.Length == 1)
            {
                self = new MainClass(args[0]);
            }
            else
            {
                self = new MainClass();
            }
            self.run().Wait();
        }

        public MainClass()
        {
            api = new FirApi.Api();
        }

        public MainClass(string url)
        {
            api = new FirApi.Api(url);
        }
        #endregion

        public async Task login()
        {
            Console.Write("Username: ");
            username = Console.ReadLine();
            try
            {
                token = await api.auth(username);
            }
            catch (ApiException)
            {
                Console.WriteLine("Account not found.");
                Console.Write("Create account (y/n): ");
                string answer = Console.ReadLine();
                if (answer.Length > 0 && answer.Substring(0, 1).ToLower() == "y")
                {
                    try
                    {
                        token = await api.join(username);
                    }
                    catch (ApiException)
                    {
                        Console.WriteLine("Failed to create account.");
                    }
                }
            }
        }

        public async Task<List<Game>> list_games()
        {
            Console.WriteLine("Logged in as: " + username + " (" + token.Player_ID + ")");
            List<Game> games = await api.games(token.Player_ID);
            Console.WriteLine("You have " + games.Count + " games.");
            foreach (Game game in games)
            {
                Console.WriteLine(" * " + game.Game_ID + ": " + game.Player1 + " vs " + game.Player2 + " @ " + game.Status);
            }
            return games;
        }

        public async Task run()
        {
            Console.WriteLine("Welcome to Four-In-Row by puggan.");
            while (token == null)
            {
                await login();
            }

            while(true)
            {
                List<Game> games = await list_games();
                Console.WriteLine("What do you want to do now?");
                if(games.Count > 0)
                {
                    Console.WriteLine("* Enter a number to select one of the games above.");
                }
                Console.WriteLine("* Enter \"w\" to wait for a game where its your turn.");
                Console.WriteLine("* Enter \"c\" to chalange someone.");
                Console.WriteLine("Option: ");
                string option = Console.ReadLine();
                Console.Clear();

                Console.WriteLine("Sorry, not implemented");
                Console.WriteLine("");
            }
        }
    }
}
