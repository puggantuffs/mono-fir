﻿using System;
namespace FirApi
{
    public class Debug
    {
        public static async System.Threading.Tasks.Task<string> TestAsync()
		{
			String url = "https://fir.tuffsruffs.se/api/game/1";
			var ajax = new System.Net.Http.HttpClient();
			return await ajax.GetAsync(url).Result.Content.ReadAsStringAsync();
		}
    }
}
